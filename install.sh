#!/bin/bash

# Define Variables for Syntax Highlighting Colors
C_DEFAULT='\033[m'
C_CYAN='\033[36m'
C_LIGHTGREEN='\033[1;32m'

printf "\n⏳ ${C_CYAN}Installing Code Development Environment Dependencies${C_DEFAULT}\n"
brew install nginx shivammathur/php/php@8.2
brew link php@8.2 --force --overwrite
brew tap shivammathur/extensions
brew install shivammathur/extensions/igbinary@8.2
brew install shivammathur/extensions/msgpack@8.2
brew install shivammathur/extensions/redis@8.2
brew install git node npm
brew install --ignore-dependencies composer

printf "\n⏳ ${C_CYAN}Installing Laravel Valet Web Server${C_DEFAULT}\n"
composer global require laravel/valet
mkdir ~/Code
valet install
cp /opt/homebrew/etc/openssl@3/cert.pem /opt/homebrew/etc/openssl@3/cert.pem.bak && cat ~/.config/valet/CA/LaravelValetCASelfSigned.pem >> /opt/homebrew/etc/openssl@3/cert.pem
cd ~/Code && valet park
valet restart

# Debug PHP Configuration and Enabled Extensions (ex. Redis)
# php -m

printf "\n⏳ ${C_CYAN}Installing desktop applications${C_DEFAULT}\n"
brew install dbngin tableplus tinkerwell

printf "\n⏳ ${C_CYAN}Adding alias and paths to ZSH Configuration${C_DEFAULT}\n"

echo -e '' >> ~/.zshrc
echo -e '# Homebrew Paths' >> ~/.zshrc
echo -e 'export PATH="$PATH:$HOME/bin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/usr/local/bin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/usr/local/sbin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/opt/homebrew/bin"' >> ~/.zshrc
echo -e '# END Homebrew Paths' >> ~/.zshrc
echo -e '' >> ~/.zshrc
echo -e '# Laravel/PHP Development Paths' >> ~/.zshrc
echo -e 'export COMPOSER_MEMORY_LIMIT=-1' >> ~/.zshrc
echo -e 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> ~/.zshrc
echo -e '# END Laravel/PHP Development' >> ~/.zshrc
echo -e '' >> ~/.zshrc
echo -e '# Laravel/PHP Development Aliases' >> ~/.zshrc
echo -e 'alias art="php artisan"' >> ~/.zshrc
echo -e 'alias unit="vendor/bin/phpunit"' >> ~/.zshrc
echo -e 'alias pesty="vendor/bin/pest"' >> ~/.zshrc
echo -e '# END Laravel/PHP Development' >> ~/.zshrc

zsh
source ~/.zshrc

printf "\n\n✅ ${C_LIGHTGREEN}Configuration complete. You are on your way to becoming an Artisan! Go build something amazing!${C_DEFAULT}\n"
