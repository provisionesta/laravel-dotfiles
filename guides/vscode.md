# Visual Studio (VS) Code

## User Configuration Steps

### VS Code Settings Profile

1. Open VS Code.

1. In the bottom left corner, click the user avatar and choose **Turn on Settings Sync**.

1. In the top center, you will be prompted to sign in with GitHub. Follow the prompts to authenticate with GitHub.

1. In the top menu bar, click on **Code** and navigate to **Settings > Profiles > Import Profile**. Click **Select File** and navigate to your **Downloads** folder and choose the `my-vscode.code-profile` file that was downloaded by the Dotfiles scripts.

1. Name the profile `{firstInitial}{lastName}-profile`.

1. You will see the status in the bottom right corner. **It will take several minutes for all the extensions to download.**

1. Press `Cmd+Shift+P` and type in `settings.json` and choose `Preferences: Open User Settings (JSON)`.

1. Find your MacOS username in Finder. Change the two `"$HOME/Code"` to `/Users/{macUsername}/Code`. Usually this will be `/Users/{firstInitial}{lastName}/Code`.

1. Make any other tweaks that you want (ex. `editor.fontFamily`, `editor.fontSize`, `editor.fontWeight`, `editor.lineHeight`).

1. Press `Cmd+S` to save the file and see your changes applied.

### VS Code Color Theme

1. In the right sidebar, click the stacked folder icon (Project Manager).

1. Click on the `{repo-name}` project under the `Git` section.

    > The external link arrow icon to the right of each project will open that project in a new VS Code window.

1. In the right sidebar, click the single folder icon to view the source code directory tree.

1. In the directory tree, expand the `routes` folder and open the `web.php` file.

1. You can press `Cmd+Shift+P` and type `Color Theme`. After selecting `Preferences:Color Theme`, you will see a list of themes. Use the up and down arrows on your keyboard (without pressing `Enter/Return`) to see a live preview of the theme. You can install additional themes as you see fit. Press `Enter/Return` when you have found a theme that you want to use.

1. Verify that the syntax formatting/styling looks good to you. You can use `Cmd+Shift+P` and `settings.json` again to make real time tweaks.

### VS Code Authoring

1. In the directory tree, double click on the `CONTRIBUTING.md` file to open it.

1. Press `Cmd+Shift+P` and type `Markdown Preview` then select `Markdown Preview Enhanced: Open Preview to the Side`. You can make changes in real time and see the rendered Markdown.

1. Press `Cmd+T` to open the integrated terminal.

1. Enable HTTPS with LetsEncrypt using Laravel Valet.

        cd ../

        valet secure {repo-name}

        cd {repo-name}

1. In the integrated terminal, click the plus button to create a **new terminal instance**. Run the following command to get started with your Laravel application.

        composer install

        php artisan key:generate

        npm install

        npm run dev

1. In the **first** Terminal window, navigate back into your repository and view the list of console commands. In the ZSH aliases, we configured `art` to be an alias for `php artisan`.

        art

        art route:list

1. Open your web browser to `https://{repo-name}.test`. You should now be able to preview this site in your Chrome browser. Due to automatic refresh with NPM tooling, your browser should auto refresh every time that you save a file to see a live preview.

1. See the `INSTALL.md` and `CONTRIBUTING.md` in each repository for more details on how to contribute code and configure any database settings before running `php artisan migrate` to populate your database.

1. You're up and running with Visual Studio (VS) Code. All the other settings are personal preferences and feel free to tweak and install more extensions as needed.
