# TablePlus Database Client

[TablePlus](https://tableplus.com/) is a MySQL and PostgreSQL database client UI that is similar to Sequel Pro, BeaverDB, DataGrip, MySQL Workbench, and others. It integrates natively with DBngin, our preferred local database server. The free version works fine with a 2 tab limit.

## User Configuration Steps

1. This assumes that you've opened TablePlus from DBngin. If not, open DBngin and click the arrow to the right of the `Stop` button and `Open in TablePlus`.

1. In the menu bar at the top of your monitor, choose **Connection > Edit**. Click **OK** to close the workspace.

1. In the PostgreSQL connection modal window, leave all values at the default and click the **Save** button. The TablePlus window will close.

1. Click on the TablePlus icon in your dock to open the connections window.

1. Double click on the `{repo-name}-pgsql` database.

1. In the `{repo-name}-pgsql` database server window, use one of the following options to open the database management window.

    - Click the database icon at the top of the window.
    - Press `Cmd+K`.
    - Use the menu bar to navigate to **Connection > Open a database**.

1. In the Open database modal window, click the **New** button.

    - Name: {repo-name}
    - Encoding: Default
    - Collation: Default

1. Double click on the `{repo-name}` database in the list.

1. Your database is now ready to use and you can access the tables using TablePlus at any time. You will run migrations and seeders to populate your database in an upcoming task.

1. Close the TablePlus window. You can exit (`Cmd+Q`) this application.

1. Close the DBngin window. It is not recommended to `Cmd+Q` this application if you are doing active development since it will stop the database service.
