# Fork Git Client

[Fork](https://fork.dev/) is our opinionated recommended tool for Git commits and branch management on your local machine. If you prefer to use Tower, GitKraken, VS Code commits, or another tool, you are welcome to do so.

```bash
brew install fork
```

## User Configuration Steps

1. Open the **Fork** app.

1. In the menu bar, navigate to **Fork > Settings**.

1. On the **General** tab, change the `Default Source Folder` to the `Code` folder in your home directory.

1. (Optional) Change the font to your preferred monospace font. Feel free to download other monospace fonts.

1. In the **Appearance** section, check the box for `Automatically push on commit`.

1. On the **Git** tab, update the **Global User Information** with your first and last name (ex. `Dade Murphy`) and work email address (ex. `dmurphy@example.com`).

1. On the **Integration** tab, use the following settings:

    - Terminal Client: `iTerm2`
    - Merge Tool: `VSCode`
    - External Diff Tool: `VSCode`.

1. Close the **Preferences** window.

1. In the menu bar, navigate to **Fork > Accounts**.

1. Add a new account for `GitLab`. The `GitLab Server` is for self-managed instances and `GitLab` is for GitLab.com SaaS. Paste the `Personal Access Token` from 1Password that you saved in the previous task.

1. In the GitLab account and Repositories tab, you will see a list of all available repositories. Click the download arrow on the right side of each row to clone each repository that you want to work with in the near future. You can leave all values at their default. You don't need to choose the `Account` from the list (recursive UI bug).

1. In the left sidebar, right click on the **Repositories** section and choose **Rescan repositories**. You may need to do this if you clone a new repository and it's not listed.

1. Double click on one of the repositories that you will work with in the future. You will see all Git commits related to this repository.

1. At the top of the Fork window, click the **Quick Launch** icon button and type in the name of the repository that you want to work with today. Choose the folder from the auto populated list.

1. In the left sidebar, left click on `Branches > main`. Note that this is different than `Remotes > origin > main`. Click the **Fetch** icon button at the top of the window, followed by the **Pull** icon button.

    > This needs to be performed each time you want to make changes to the code base to ensure you have the latest working copy. If you see a number with a down arrow to the right of `main` then it means that commits are available on the server that you need to pull down.

1. Right click on `Branches > main`  and choose **New Branch**. In the branch name, type in `feature/add-{feature-name}`.

1. Fork will automatically switch the repository to this branch and it can be edited using VS Code. You can return to the Fork window when you're ready to commit your changes.

1. Close the Fork window.
