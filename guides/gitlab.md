# GitLab

These instructions assume that you have not performed Git commits on your local machine before and are going to be configuring [Fork](fork.md), our opinionated preference for a Git client. If you already perform commits with a Git client of your choice, you can skip these steps.

## User Configuration Steps

### GitLab User Profile

1. Navigate to https://gitlab.com and sign in with the credentials in your 1Password vault.

1. Click on your avatar and choose **Preferences**.

1. Navigate to **Profile** in the left sidebar.

### Personal Access Tokens

> **Security Warning:** For security reasons, you should never re-use an Access Token for multiple applications or services. you should create a new access token with a descriptive service-specific name for each use case. Never use a personal access token for your user account on any server or deployed service. You should also use less permissive [group access tokens](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) and [project access tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) when possible. If you are working with Laravel applications, see the [provisionesta/gitlab-api-client](https://gitlab.com/provisionesta/gitlab-api-client/-/blob/main/README.md?ref_type=heads#security-best-practices) README for more information.

1. Navigate to **Access Tokens** in the left sidebar.

1. Add a new personal access token named `fork` with the `api` and `write_repository` scopes.

1. Edit the 1Password record for your username and add a new field with password type named `API Token - fork`. Copy the value from the GitLab UI.

### SSH Key

1. Navigate to **SSH Keys** in the left sidebar.

1. Add a new SSH key and autofill the contents from your 1Password record `SSH Key - gitlab.com - {handle}`.

1. Leave the Usage type as `Authentication & Signing`.

1. Leave the Expiration date as the default value. This 1-year value meets our security policies.

1. Click **Add Key**.

1. In 1Password, edit the SSH key record to add a new `Date` field and rename it to `expires_at` then fill in the date value to match the expiration date in GitLab's UI.

1. Close the browser tab with GitLab.

> **Security Reminder: No Keys in ~/.ssh**
> Just a reminder that any commands that use SSH will prompt you for unlocking 1Password and/or using Touch ID. You should not store any SSH public/private key files in `~/.ssh`.
