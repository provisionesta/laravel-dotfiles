# 1Password

This assumes that you already have 1Password installed and just need to enable the 1Password CLI and SSH agent.

## User Configuration Steps

### 1Password CLI and SSH Agent

> [https://developer.1password.com/docs/cli/get-started/#install](https://developer.1password.com/docs/cli/get-started/#install)
>
> These steps assume that the 1Password CLI was installed and the `~/.ssh/config` file was copied when you ran the dot files scripts.

1. Open iTerm and run the following `brew` commands.

    ```plain
    brew install 1password 1password-cli
    ```

1. Type `op --version` to ensure that 1Password CLI is installed.

1. In the top left corner of the 1Password application window, click on `{Org Name}` and choose **Settings** from the dropdown menu. Navigate to **Developer** in the left sidebar.

1. Enable the `Integrate with 1Password CLI` checkbox.

1. Click the **Set Up SSH Agent** button and choose `Use Key Names`.

1. When the code snippet appears, click the `X` button to close it. This has already been pre-configured by the Dotfiles script.

1. Change **Remember key approval** to `for 12 hours`.

1. You can close the 1Password window.

1. In iTerm, run `op vault list`.

1. Watch this [video](https://www.youtube.com/watch?v=7aT4K1AMfGI) to learn more about how you'll use the 1Password CLI for securely storing secrets that would normally be configured in your terminal or in environment variables.

1. You can configure your credentials and environment variables later for each of the CLI services including [AWS](https://developer.1password.com/docs/cli/shell-plugins/aws), [GitLab](https://developer.1password.com/docs/cli/shell-plugins/gitlab), [Okta](https://developer.1password.com/docs/cli/shell-plugins/okta), [PostgreSQL](https://developer.1password.com/docs/cli/shell-plugins/postgresql), [Terraform](https://developer.1password.com/docs/cli/shell-plugins/terraform/), etc.

### 1Password SSH Keys

1. **Open 1Password** from  your Dock.

1. With your `Private` vault select in the left sidebar, click **New Item** in the top right corner.

1. Choose **SSH Key** from the available options.

1. Change the **title** to `Private SSH - gitlab.com - {handle}`.

1. Click **Add Private Key** and choose **Generate a New Key**.

1. Select the `Ed25519` Key Type and click **Generate**.

1. Click **Save** in the bottom right corner to save the 1Password record.

### Future Reference

- For security purposes, **every website needs to use a different SSH key**. You can repeat these steps to generate additional SSH keys as needed using `Private SSH - {website} - {usernme}` title format.
- You will also find several SSH keys in shared vaults. These will be prefixed with `Shared SSH` instead of `Private SSH`.
- **Shared SSH keys are rotated periodically** and since we use 1Password SSH Agent, **you do not need to do anything to stay up-to-date with the latest shared keys**.
