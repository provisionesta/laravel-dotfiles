# DBngin Database Server

[DBngin](https://dbngin.com/) is a database server provisioning tool on your local Mac for creating MySQL, Postgres or Redis database instances without any installation headaches. Trust us, you do not want to deal with installing and configuring databases with `brew` if you can help it.

## PostgreSQL Server Configuration Steps

> Replace `{repo-name}` with the name of your repository folder. You can choose any name you'd like, this just keeps it easy to name things.

1. Open the `DBngin` application.

2. At the top of the window, check the `Launch on Login` and `Open in menu bar` checkboxes. This allows you to use a simpler view up by your clock without having to open the desktop application.

3. Click the `+` plus button to the left of `Launch on Login`.

4. In the **Create new database server** window, use the following settings.

    - Service: `PostgreSQL`
    - Version: `15.1` (or latest)
    - Name: `laravel-pgsql`
    - Check the box to `Automatically start service on login`.
    - Leave all other settings at their default value.
    - Click the **Create** button.

5. In the list of databases, click the **Start** button next to `laravel-pgsql`. It will take a few moments for it to download binaries.

6. Open your Terminal and create a new database (data store) on the new database server.

    ```bash
    psql -U postgres
    ```

    ```bash
    CREATE DATABASE {repo-name};
    ```

7. In DBngin, double click on the new Postgres server to open it in Table Plus.

8. The default database name is `postgres`. You can choose the new `{repo-name}` database from the database (pancake icon) in the top of the application or from `Connection > Open a Connection (Cmd+K)` in the menu bar.

9. Update your `.env` file with the following variables.

    ```plain
    DB_CONNECTION=pgsql
    DB_HOST=127.0.0.1
    DB_PORT=5432
    DB_DATABASE={repo-name}
    DB_USERNAME=postgres
    DB_PASSWORD=
    ```

## Redis Server Configuration Steps

1. Click the `+` plus button to the left of `Launch on Login`.

2. In the **Create new database server** window, use the following settings.

    - Service: `Redis`
    - Version: `7.0.0` (or latest)
    - Name: `laravel-redis`
    - Check the box to `Automatically start service on login`.
    - Leave all other settings at their default value.
    - Click the **Create** button.

3. Click on the right arrow next to the PostgreSQL Start/Stop button and choose `Open in TablePlus`.

4. The default Redis database values in the `.env` file should already be set to the values of your local Redis server. You simply need to have Laravel use the Redis database for each of the services. Update your `.env` file to replace the values for the following variables.

    ```plain
    CACHE_STORE=redis
    QUEUE_CONNECTION=redis
    SESSION_DRIVER=redis
    ```
