# Development Environment Configuration for Laravel Applications

Dotfiles are another name for Bash/Shell scripts that configure your computer how you want it without having to perform a lot of manual steps in the UI. You also don't need to navigate to the vendor websites to download any of the applications.

Run these commands one at a time and monitor the progress for any errors. You may be prompted for your local Mac password at any point.

## Dot Files Installation Steps

> These instructions are written for Apple Silicon (M#) chipset machines. Your experience on Intel Macs may differ. This guide does not cover Windows or Linux environments.

### Homebrew Package Manager

[Homebrew](https://brew.sh/) (referred to as `brew`) is a CLI package manager that allows you to easily install and compile dependencies and applications on MacOS. If you’ve ever used `apt install` or `rpm install` on Linux, or `npm install` in software engineering, it’s the same sort of thing.

1. Open **Launchpad** from your dock and type in `Terminal` in the search bar.

2. In the new Terminal window, copy and paste (referred to as copypasta) the code below and run the command.

    ```plain
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    ```

3. Add Homebrew to your ZSH configuration and path

    ```plain
    (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> $HOME/.zprofile
    ```

    ```plain
    eval "$(/opt/homebrew/bin/brew shellenv)"
    ```

### Oh-My-ZSH

> These steps are optional and provide an opinionated configuration.

[Oh-My-ZSH](https://ohmyz.sh/) adds superpowers to your Terminal including themes, plugins, and a great user experience with tab completion among other things.

1. In the Terminal window, run the following command.

    ```plain
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    ```

2. (Optional) You can use the default configuration or customize it to your liking. If you want to use an opinionated configuration to get started, install the following plugins and update the variables in your `~/.zshrc` file using `nano ~/.zshrc` (or VI/VIM if you prefer).

    ```plain
    brew install autojump fzf wget grep
    ```

    ```bash
    # nano ~/.zshrc

    # Overwrite the existing ZSH_THEME variable
    ZSH_THEME="refined"

    # Add to or overwrite the plugins variable
    plugins=(
    autojump
    gcloud
    sublime
    terraform
    ubuntu
    vscode
    zsh-interactive-cd
    git
    )
    ```

    ```bash
    echo -e '[ -f /opt/homebrew/etc/profile.d/autojump.sh ] && . /opt/homebrew/etc/profile.d/autojump.sh' >> ~/.zshrc
    ```

    ```bash
    source ~/.zshrc
    ```

### Automated Script

You can install each dependency and application manually (see below), or run this automated script to streamline the process to automatically install all of your Laravel development [dependencies](#dependencies), [Valet web server](#larvel-valet), and [desktop applications](#desktop-applications).

```bash
/bin/bash -c "$(curl -fSL https://gitlab.com/api/v4/projects/53652396/repository/files/install.sh/raw\?ref=main)"
```

If you want to customize this script, simply create a new `laravel.sh` file on your computer (ex. ~/Desktop or ~/Downloads) and copy the following script into it. You can make any changes before you run it.

```bash
cd ~/Desktop
./laravel.sh
```

```bash
#!/bin/bash

# Define Variables for Syntax Highlighting Colors
C_DEFAULT='\033[m'
C_CYAN='\033[36m'
C_LIGHTGREEN='\033[1;32m'

printf "\n⏳ ${C_CYAN}Installing Code Development Environment Dependencies${C_DEFAULT}\n"
brew install nginx shivammathur/php/php@8.2
brew link php@8.2 --force --overwrite
brew tap shivammathur/extensions
brew install shivammathur/extensions/igbinary@8.2
brew install shivammathur/extensions/msgpack@8.2
brew install shivammathur/extensions/redis@8.2
brew install git node npm
brew install --ignore-dependencies composer

printf "\n⏳ ${C_CYAN}Installing Laravel Valet Web Server${C_DEFAULT}\n"
composer global require laravel/valet
mkdir ~/Code
valet install
cd ~/Code && valet park
cp /opt/homebrew/etc/openssl@3/cert.pem /opt/homebrew/etc/openssl@3/cert.pem.bak && cat ~/.config/valet/CA/LaravelValetCASelfSigned.pem >> /opt/homebrew/etc/openssl@3/cert.pem
valet restart

# Debug PHP Configuration and Enabled Extensions (ex. Redis)
# php -m

# For each site that you want to secure, you need to do so individually
# cd ~/Code/my-project-name
# valet secure

printf "\n⏳ ${C_CYAN}Installing desktop applications${C_DEFAULT}\n"
brew install dbngin tableplus tinkerwell

printf "\n⏳ ${C_CYAN}Adding alias and paths to ZSH Configuration${C_DEFAULT}\n"

echo -e '' >> ~/.zshrc
echo -e '# Homebrew Paths' >> ~/.zshrc
echo -e 'export PATH="$PATH:$HOME/bin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/usr/local/bin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/usr/local/sbin"' >> ~/.zshrc
echo -e 'export PATH="$PATH:/opt/homebrew/bin"' >> ~/.zshrc
echo -e '# END Homebrew Paths' >> ~/.zshrc
echo -e '' >> ~/.zshrc
echo -e '# Laravel/PHP Development Paths' >> ~/.zshrc
echo -e 'export COMPOSER_MEMORY_LIMIT=-1' >> ~/.zshrc
echo -e 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> ~/.zshrc
echo -e '# END Laravel/PHP Development' >> ~/.zshrc
echo -e '' >> ~/.zshrc
echo -e '# Laravel/PHP Development Aliases' >> ~/.zshrc
echo -e 'alias art="php artisan"' >> ~/.zshrc
echo -e 'alias unit="vendor/bin/phpunit"' >> ~/.zshrc
echo -e 'alias pesty="vendor/bin/pest"' >> ~/.zshrc
echo -e '# END Laravel/PHP Development' >> ~/.zshrc

zsh
source ~/.zshrc

printf "\n\n✅ ${C_LIGHTGREEN}Configuration complete. You are on your way to becoming an Artisan! Go build something amazing!${C_DEFAULT}\n"
```

## Manual Installation

### Dependencies

> You can automatically perform all of these tasks using the [automated script](#automated-script).

1. **Git**

    > You can skip this if you already have `git` installed. This command will upgrade to the latest version.

    ```bash
    brew install git
    ```

2. **Node.js and Node Package Manager (NPM)**

    > You can skip this if you already have `node` installed. This command will upgrade to the latest version.

    ```bash
    brew install node npm
    ```

3. **PHP v8.2** (using `shivammathur/php` tap)

    ```bash
    brew install shivammathur/php/php@8.2
    ```

    ```bash
    brew link php@8.2 --force --overwrite
    ```

4. **PHP extensions** (using [shivammathur/extensions](https://github.com/shivammathur/homebrew-extensions) tap)

    ```bash
    brew tap shivammathur/extensions
    brew install shivammathur/extensions/igbinary@8.2
brew install shivammathur/extensions/msgpack@8.2
    brew install shivammathur/extensions/redis@8.2
    ```

5. **[Composer](https://getcomposer.org/)** (package manager for PHP)

    > **Version Note:** Composer will install the latest PHP version from Homebrew as a dependency if you do not have PHP installed. By installing it above with a specific tap, you have more control of which version of PHP is installed.

    ```bash
    brew install composer
    ```

6. **Add PATH directories**

    ```bash
    echo -e '' >> ~/.zshrc
    echo -e '# Homebrew Paths' >> ~/.zshrc
    echo -e 'export PATH="$PATH:$HOME/bin"' >> ~/.zshrc
    echo -e 'export PATH="$PATH:/usr/local/bin"' >> ~/.zshrc
    echo -e 'export PATH="$PATH:/usr/local/sbin"' >> ~/.zshrc
    echo -e 'export PATH="$PATH:/opt/homebrew/bin"' >> ~/.zshrc
    echo -e '# END Homebrew Paths' >> ~/.zshrc
    echo -e '' >> ~/.zshrc
    echo -e '# Laravel/PHP Development Paths' >> ~/.zshrc
    echo -e 'export COMPOSER_MEMORY_LIMIT=-1' >> ~/.zshrc
    echo -e 'export PATH="$PATH:$HOME/.composer/vendor/bin"' >> ~/.zshrc
    echo -e '# END Laravel/PHP Development' >> ~/.zshrc
    ```

7. **Command Aliases**

    > These are **optional** and can be customized to your liking.

    ```bash
    echo -e '' >> ~/.zshrc
    echo -e '# Laravel/PHP Development Aliases' >> ~/.zshrc
    echo -e 'alias art="php artisan"' >> ~/.zshrc
    echo -e 'alias unit="vendor/bin/phpunit"' >> ~/.zshrc
    echo -e 'alias pesty="vendor/bin/pest"' >> ~/.zshrc
    echo -e '# END Laravel/PHP Development' >> ~/.zshrc
    ```

### Web Server

[Laravel Herd](https://herd.laravel.com/) provides an all-in-one development environment with a single application and no environment dependencies.

This is a great starting point if you are a new developer and do not work in multiple languages that may have conflicting web servers, databases, or services running on your machine.

If you need a more flexible configuration or want to manage the components yourself, check out [Laravel Valet](#laravel-valet) that installs components with Homebrew. If you'd like to run your development server in a local Docker container, check out [Laravel Sail](#laravel-sail).

#### Laravel Sail

Laravel Sail provides a light-weight command-line interface **using Docker** on your local machine for running your development web server.

See the [Laravel Sail](https://laravel.com/docs/11.x/sail) documentation to learn more.

#### Laravel Valet

[Laravel Valet](https://laravel.com/docs/10.x/valet) provides an all-in-one web server (similar to the old days of WAMP/MAMP, and the precursor to Laravel Herd) that **uses Homebrew** to install and manage Nginx, PHP FPM, automatic Nginx Virtual Hosts for your code repository directories, and uses dnsMasq for fake domain names for each of your repositories.

This allows you to clone a repository to `~/Code/my-cool-project` and instantly visit `http://my-cool-project.test` without any web server configuration.

You can use `cd ~/Code/my-cool-project && valet secure` to have a LetsEncrypt certificate managed automatically for you so you can visit `https://my-cool-project.test`.

```bash
composer global require laravel/valet
```

```bash
valet install
```

```bash
cp /opt/homebrew/etc/openssl@3/cert.pem /opt/homebrew/etc/openssl@3/cert.pem.bak && cat ~/.config/valet/CA/LaravelValetCASelfSigned.pem >> /opt/homebrew/etc/openssl@3/cert.pem
```

```bash
# (Optional) This should only be run if you do not have any other code projects on your computer.
mkdir ~/Code/
```

```bash
# This example assumes that your projects are in ~/Code/.
# You can change this to where your Git repositories are cloned to.
cd ~/Code/
valet park
valet restart
```

## Your First Project

You can test if everything is working by creating a new project on your local machine in the folder where you configured your web server to watch for new projects in.

1. Using your Terminal, change to your code directory.

    > **Reminder:** This example assumes that your projects are in `~/Code/`. You can change this to where your Git repositories are cloned to. Laravel Herd applications are configured in `~/Herd/` by default.

    ```bash
    cd ~/Code/
    ```

2. Create your first Laravel project using the [installation instructions](https://laravel.com/docs/11.x/installation).

    ```bash
    composer global require laravel/installer
    ```

    ```bash
    laravel new my-first-app
    ```

    > **Note:** The `composer create-project laravel/laravel {name}` command does not include the setup wizard. The Laravel installer is recommended if you're new to Laravel.

3. Select the [Laravel Breeze](https://laravel.com/docs/11.x/starter-kits#laravel-breeze) starter kit.

4. Select a start kit variation based on your existing knowledge of backend and frontend technologies.

    > If you're a backend developer and are comfortable with HTML, CSS, and using variables, choose [Blade](https://laravel.com/docs/11.x/blade) with [Alpine](https://alpinejs.dev/). If you're new to web application development, choose this option.
    >
    > If you're a frontend or fullstack developer using React or Vue.js, you can choose React or Vue with [Inertia](https://inertiajs.com/).
    >
    > As you learn more about Laravel, you should create multiple test projects with each of the different starter kits to learn more about their benefits and differences. You should also take time to look at [Livewire](https://livewire.laravel.com/) documentation and create a test project to see if it's a good fit for you.

5. Select the `Pest` testing framework option.

6. The default database option is SQLite. You can also choose one of the other database options. You should choose `SQLite` if you're new to Laravel and you can update your `.env` file later and re-run your [database migrations](https://laravel.com/docs/11.x/migrations).

    > You should choose SQLite if you're just getting started, or MySQL/MariaDB or PostreSQL if you plan to use those databases in production. See the [DBngin Database Server](#dbngin-database-server) section for instructions on setting up your local database server.

7. For this test project, it is not necessary to initialize a Git repository.

    > You can initialize a Git repository during the Laravel installation, or create your repository first and create the Laravel project inside of it.

    ```bash
    # This is for reference only. Do not run this unless you have an existing repository.
    cd ~/Code
    git clone https://github.com/{my-namespace}/{my-new-repo}.git
    cd {my-new-repo}
    laravel new . --force
    ```

8. Review the CLI outputs from the installation.

    > Laravel has Composer (PHP) and Node.js (NPM) dependencies. You can think of these as backend and frontend JavaScript dependencies respectively. Most package names correspond to the respective `https://github.com/{namespace}/{package-name}` if you want to explore the README or source code.
    >
    > During the installation process, you saw a list of several Composer packages for the core Laravel framework being installed. You can see all of the Composer packages listed in `composer.json` and any inherited dependencies in `composer.lock`. Composer packages are installed into the `vendor` folder of your application. You can explore all of the available Composer packages on [packagist.org](https://packagist.org/).
    >
    > Node packages are installed into the `node_modules` folder of your application and are managed in `package.json`. You can explore all of the available NPM packages on [npmjs.org](https://www.npmjs.com/).
    >
    > [Vite](https://vitejs.dev/) is used to process your JavaScript dependencies and build the `public/build/assets/app-{hash}.js` file, including your [Tailwind CSS](https://tailwindcss.com/docs/installation) configuration and styles.

9. Change directory into the project that you just created.

    ```bash
    cd ~/Code/my-first-app
    ```

10. Run the php artisan command to see all of the command-line interface Console commands.

    ```bash
    php artisan
    ```

    ```bash
    php artisan route:list
    ```

    > If you configured an alias in your `.zshrc` file, you can use it easily here.

    ```bash
    # alias art="php artisan"
    art route:list
    ```

11. Open the source code in your preferred editor.

    ```bash
    # Visual Studio Code
    code .
    ```

    ```bash
    # PHP Storm
    phpstorm .
    ```

    ```bash
    # Sublime Text
    subl .
    ```

12. Open the following files in your web editor to see how Laravel loads the web page.

    > **Reminder:** You may need to double click the file name in the editor to keep it open when opening multiple tabs. A single click may only show a preview and close the tab when you choose another file.

    ```bash
    routes/web.php
    resources/views/welcome.blade.php
    .env
    ```

    ```php
    // routes/web.php

    Route::get('/', function () {
        return view('welcome');
    });
    ```

### View your Website

#### View Website with Laravel Valet

```bash
# Only need to run on first installation to set up LetsEncrypt certificate
valet secure
```

You can access the website at any time in your browser. Simply open a new tab to `https://my-first-app.test`.

You can also easily open the web browser to this URL from your Terminal.

```bash
valet open
```

See the [Valet documentation](https://laravel.com/docs/11.x/valet) to learn more.

#### View Website with Laravel Herd

```bash
herd open
```

See the [Herd documentation](https://herd.laravel.com/docs/1/getting-started/sites) to learn more.

#### View Website with Laravel Sail

```bash
./vendor/bin/sail up
```

See the [Sail documentation](https://laravel.com/docs/11.x/sail) to learn more.

#### View Website with Laravel Built-in Development Server

```bash
php artisan serve
```

### Navigate to Registration Page

1. In the top right corner of your browser, click the **Register** link. You will notice that the path in the URL has changed to `/register`.

2. In your `routes/web.php`, you will notice that this is missing. This is because Breeze has included it as part of a package and is not something that you should need to adjust when using the starter kit. You can view all routes that are registered with `php artisan route:list`.

    ```plain
    GET|HEAD  register ....... register › Auth\RegisteredUserController@create
    ```

3. You can customize the view in `resources/views/auth/register.blade.php`. Locate the Email address form field and change the display value from `Email` to `Email Address`.

    ```diff
    - <x-input-label for="email" :value="__('Email')" />
    + <x-input-label for="email" :value="__('Email Address')" />
    ```

4. Save the file and refresh your browser page. You will notice that the second form field has changed.

5. Create a (fake test) user account for yourself and proceed to the dashboard page.

### Your First CRUD Resource

The base dashboard provides a scaffolding for you to get started with. Laravel uses a model, view, controller (MVC) architecture that you can learn more about in the documentation.

This command will help you see the core scaffolding for your first Create-Read-Update-Delete (CRUD) model, view, and controller.

```bash
php artisan make:model EventTicket --migration --controller --resource
```

Try out `php artisan make` to see all of the helper commands that you can use to generate your code scaffolding.

You will spend most of your time in the first few weeks working with the following concepts:

<table>
<thead>
<tr>
<th>Docs (File Path)</th>
<th><a target="_blank" href="https://laravel.com/docs/11.x/artisan">Artisan Console</a> Commands</th>
</tr>
</thead>
<tbody>
<!-- Models -->
<tr>
<td>
<a target="_blank" href="https://laravel.com/docs/11.x/eloquent">Models</a><br>
<code>app/Models/*</code>
</td>
<td>
<code>php artisan make:model --help</code><br>
</td>
</tr>
<!-- Migrations -->
<tr>
<td>
<a target="_blank" href="https://laravel.com/docs/11.x/migrations">Migrations</a><br>
<code>database/migrations/*</code>
</td>
<td>
<code>php artisan make:migration --help</code><br>
<code>php artisan make:model {ModelName} --migration</code><br>
</td>
</tr>
<!-- Controllers -->
<tr>
<td>
<a target="_blank" href="https://laravel.com/docs/11.x/controllers">Controllers</a><br>
<code>app/Http/Controllers/*</code>
</td>
<td>
<code>php artisan make:controller --help</code><br>
<code>php artisan make:model {ModelName} --controller --resource</code><br>
</td>
</tr>
<!-- Routing -->
<tr>
<td>
<a target="_blank" href="https://laravel.com/docs/11.x/routing">Routing (Routes)</a><br>
<code>routes/web.php</code>
</td>
<td>
<code>php artisan make:controller --help</code><br>
<code>php artisan make:model {ModelName} --controller --resource</code><br>
</td>
</tr>
<!-- Requests -->
<tr>
<td>
<a target="_blank" href="https://laravel.com/docs/11.x/requests">Requests</a><br>
<code>app/Http/Requests/*</code>
</td>
<td>
<code>php artisan make:request --help</code><br>
</td>
</tr>
</tbody>
</table>

### Learn More

You may want to check out [Laravel Bootcamp](https://bootcamp.laravel.com/) and [Laracasts](https://laracasts.com/topics/laravel) to learn more tips and tricks as you get started creating your application.

One of the best ways to learn is to study an existing Laravel application if a coworker has already built one. If the Controllers folder appears lightweight, check if an `app/Actions`, `app/Jobs`, `app/Repositories`, or `app/Services` folder exists where business logic may be stored at.

If you are starting to contribute to an existing project, see the `CONTRIBUTING` file in the repository for more details. The best way to learn is by wandering around. If the maintainers have not already created any onboarding documentation for developers, you may want to ask for some training or a code pair orientation call with them to speed up your discovery process.

## Desktop Applications

You can use any desktop applications that you would like for working with your Laravel application and managing your database. Here are popular ones that many Laravel developers use.

### Visual Studio Code (VS Code)

[Visual Studio Code](https://code.visualstudio.com/) is one of the popular Integrated Development Environments (IDE). Based on the [State of Laravel 2024 results](https://stateoflaravel.com/results#question:primary+code+editor), 40% of developers use VS Code and 54% use [Jetbrains PHPStorm](https://www.jetbrains.com/phpstorm/).

If you code in a variety of languages, then you're likely familiar with using VS Code.

See the [VSCode Guide](guides/vscode.md) for configuration instructions.

#### Popular Extensions

> **Coming Soon:** The official Laravel VS Code extension is being created by the Laravel Core team and will be available soon.

- [Better Pest](https://marketplace.visualstudio.com/items?itemName=m1guelpf.better-pest)
- [Composer](https://marketplace.visualstudio.com/items?itemName=ikappas.composer)
- [Laravel Extension Pack](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-extension-pack)
- [Laravel Blade](https://marketplace.visualstudio.com/items?itemName=amirmarmul.laravel-blade-vscode)
- [Laravel Docs](https://marketplace.visualstudio.com/items?itemName=austenc.laravel-docs)
- [Laravel Extra Intellisense](https://marketplace.visualstudio.com/items?itemName=amiralizadeh9480.laravel-extra-intellisense)
- [Livewire Language Support](https://marketplace.visualstudio.com/items?itemName=cierra.livewire-vscode)
- [php cs fixer](https://marketplace.visualstudio.com/items?itemName=junstyle.php-cs-fixer)
- [PHP DocBlocker](https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker)
- [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

#### Settings

For an improved developer experience with key bindings and helpful settings, you may also want to check out [Make VS Code Awesome](https://makevscodeawesome.com/) from Caleb Porzio, the creator of Alpine.js and Livewire.

To get you started, here are a few helpful lines to add to your User Settings JSON file.

```json
    /**
    * PHP CS Fixer
    **/
    "[php]": {
        "editor.defaultFormatter": "bmewburn.vscode-intelephense-client",
        "editor.formatOnSave": true
    },
    "php-cs-fixer.onsave": true,
    "php-cs-fixer.showOutput": true,
    "php-cs-fixer.autoFixByBracket": false,
    "php-cs-fixer.rules": "@PSR12",

    /**
    * Prettier
    **/
    "[javascript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode",
        "editor.formatOnSave": true
    },
    "prettier.requireConfig": true,
    "prettier.useEditorConfig": false,
```

### DBngin Database Server

You can use SQLite by default with your Laravel application. This will create a flat file in `~/Code/{repo-name}/database/database.sqlite`. To use the databases that are used in your production environment, you can create local MySQL, Postgres, and/or Redis database servers easily using [DBngin](https://dbngin.com/) without any installation headaches.

```bash
brew install dbngin
```

See the [DBngin configuration guide](guides/dbngin.md) to create your relational database server, a new Redis server, and configure your Laravel application.

### Table Plus Database Client

[TablePlus](https://tableplus.com/) is a MySQL and PostgreSQL database client UI that is similar to Sequel Pro, BeaverDB, DataGrip, MySQL Workbench, and others. It integrates natively with DBngin, our preferred local database server. The free version works fine with a 2 tab limit.

See the [TablePlus Guide](guides/tableplus.md) for configuration instructions.

```bash
brew install tableplus
```

### Tinkerwell

You can easily experiment and test snippets of code when developing your Laravel application using Laravel Tinker. Since Laravel Tinker is instantiated with the current code in your application using `php artisan tinker` and is not refreshed if you make changes, you need to press `Ctrl+C` to terminate your Tinker session and restart it.

[Tinkerwell](https://tinkerwell.app/) is a paid tool that is very helpful for day-to-day development and testing.

```bash
brew install tinkerwell
```

### Google Chrome Extensions

There are several Chrome extensions that are helpful for debugging Laravel web applications. Install each of these from the Chrome Web Store.

- [Clockwork](https://underground.works/clockwork/)
- [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar)
- [Tailwind Devtools](https://chromewebstore.google.com/detail/tailwind-devtools/lehhmglccbkmehbbldcmekdegmapaknb?pli=1)

### Additional Guides

- [1Password CLI and Secrets](guides/1password.md)
- [Fork (Git Client)](guides/fork.md)
- [GitLab](guides/gitlab.md)
